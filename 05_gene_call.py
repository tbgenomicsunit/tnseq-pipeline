#!/usr/bin/python

import sys
import glob
import os

header = ["Gene"]
output = {}

for filename in glob.glob(os.getcwd()+"/*siteCall.csv"):

	header.append(filename.split("/")[-1].split("_")[0])

	esites = open(filename)
	data={}
	genes = []

	for line in esites:
		line = line.replace('"',"")
		line = line.strip().split(",")

		if "call" not in line:
			if line[0] not in data.keys():
				data[line[0]] = []
				data[line[0]].append(line[-1])
				genes.append(line[0])
			else:
				data[line[0]].append(line[-1])

	for gene in genes:
		if "/" not in gene:
			string = data[gene]

			nTA = len(string)
			nGA = string.count("GA")
			nGD = string.count("GD")
			nNV = string.count("NV")

			call = "NS"
			p1 = float(nGA)/float(nTA)
			p2 = float(nGD)/float(nTA)
			p3 = float(nNV)/float(nTA)

			if p1 >= 0.7:
				call = "GA" 
			if p2 >= 0.7:
				call = "GD"
			if p3 >= 0.6:
				call = "NV"
			
			if gene not in output.keys():
				output[gene]=[]
				output[gene].append(call)
			else:
				output[gene].append(call)

print ",".join(header)
for gene, calls in output.items():
	print(gene+","+",".join(calls))

