#!/usr/bin/python

import sys

TA = []
evaluated = []

with open(sys.argv[3]) as inWindows:
	for line in inWindows:
		line = line.strip().split(",")
		evaluated.append(line[0])

counts = []
genes = []

header = []

with open(sys.argv[2]) as inZscores:
	for line in inZscores:
		if line[0] != "C":
			genes.append(line.strip().split(",")[1])
			counts.append(line.strip().split(",")[header.index(sys.argv[1].split("_")[0])])
			TA.append(float(line.split(",")[0]))
		else:
			for item in line.strip().split(","):
				header.append(item.replace("_",""))

windows = []

with open(sys.argv[1]) as qresults:
	for line in qresults:
		if line[0] != "S":
			line = line.strip().split(",")
			start, end = float(line[0]), float(line[1])
			windows.append((start,end,line[-1]))

with open(sys.argv[1].replace("FDR","siteCall"),"a") as outfile:
	outfile.write("Gene,TA site,counts,#windows,#GA,#GD,call\n")

	i = 0
	for site in TA:
		call = []
		nw = 0
		for window in windows:
			if window[0] <= site <= window[1]:
				call.append(window[2])
				nw += 1

		nGA = call.count("GA")
		nGD = call.count("GD")
		total = len(call)

		fCall = "NS"

		if total != 0:
			if (float(nGA) / float(total)) > 0.5:
				fCall = "GA"

			if (float(nGD) / float(total)) > 0.5:
				fCall = "GD"

		if genes[i] not in evaluated:
			fCall = "NV"

		outfile.write(genes[i]+","+str(site).split(".")[0]+","+counts[i]+","+str(nw)+","+str(nGA)+","+str(nGD)+","+fCall+"\n")
		i+=1