#!/usr/bin/python

#This script generates windows to test TA sites along the genome
#usage: pypy windows_gen.py annotation TA_list

import sys
import collections

#we load the genome annotation into a dictionary
genes = {}
sitesDict = {}

with open(sys.argv[1]) as inAnnot:
	for line in inAnnot:
		line = line.strip().split(",")
		genes[line[0]] = [int(line[-3]),int(line[-2])]
		sitesDict[line[0]] = []

#we load the genome TA sites into a list

zscoresDict, TA_sites = {}, []

with open(sys.argv[2]) as inZscores:
	for line in inZscores:
		if line.startswith("Coord"):
			header = line
		else:
			line = line.strip().split(",")
			TA_sites.append(int(line[0]))
			zscoresDict[line[0]] = line[2:]

#we assign TA sites to every feature and save a new file containing duplicates

for gene, coords in genes.items():
	for site in TA_sites:
		if coords[0] <= site <= coords[1]:
			sitesDict[gene].append(site)

with open("Zscores_pipeline.csv","w") as outfile:
	outfile.write(header)
	for gene, TAlist in sitesDict.items():
		for TAsite in TAlist:
			outfile.write(str(TAsite)+","+gene+","+",".join(zscoresDict[str(TAsite)])+"\n")


#we create windows according to number of sites in each feature

for gene, sites in sitesDict.items():
	if 6 <= len(sites) <= 9:
		print ",".join([gene, str(sites[0]), str(sites[-1])])
	elif len(sites) > 9:
		try:
			for s in sites:
				print ",".join([gene, str(s), str(sites[sites.index(s) + 9])])
		except IndexError:
			pass
