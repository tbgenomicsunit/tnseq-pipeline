This is v1 of our TnSeq analysis pipeline tailored for Mycobacterium tuberculosis experiments.

Scripts are run in order over the insertion count files transformed into a matrix of Z-scores.