#!/usr/bin/python

import sys
import numpy as np
from scipy import stats

#we load windows to test

windows = []

with open(sys.argv[2]) as inWindows:	
	for line in inWindows:
		line = line.strip().split(",")
		windows.append((line[1],line[2]))

#we load Zscores to test

zscores = {}
TA_sites = []

with open(sys.argv[1]) as inZscores:
	for line in inZscores:
		line = line.strip().split(",")

		if line[0] != "Coord":
			TA_sites.append(int(line[0]))
			for i in range(2, len(line)):
				zscores[header[i]].append(float(line[i]))
		else:
			header = line
			for item in header:
				zscores[item] = []

#wilcoxon test of each window by sample

for sample in header[2:]:

	scores = zscores[sample]

	with open(sample.replace("_","")+"_"+sys.argv[2].split("_")[1].split(".")[0]+"_wilcox.csv","a") as outfile:
		outfile.write("Start,End,#Sites,Mean,Median,StD,p-value\n")

		for tupla in windows:
			start = int(tupla[0].split(".")[0])
			end = int(tupla[1].split(".")[0])
			coords = np.where((np.array(TA_sites) >= float(start)) & (np.array(TA_sites) <= float(end)))[0].tolist()

			if len(coords) >= 6:
				data = scores[coords[0]:coords[-1]+1]
				media = np.mean(data)
				mediana = np.median(data)
				sdev = np.std(data)
				N = len(data)

				wilcox = stats.wilcoxon(np.array(data),zero_method="zsplit")[1]

				outfile.write(tupla[0]+","+tupla[1]+","+str(N)+","+str(media)+","+str(mediana)+","+str(sdev)+","+str(wilcox)+"\n")
